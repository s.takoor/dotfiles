# Dotfiles
:wrench: This is a repository for my personal dotfiles.

# Table of Contents
[[_TOC_]]

# :rocket: Tools I use on a daily basis
- [alacritty](https://github.com/alacritty/alacritty) - Terminal
- [bat](https://docs.rs/bat/latest/bat/) - A library to print syntax highlighted content
- [conky](https://github.com/brndnmtthws/conky) - Free, light-weight system monitor for X
- [doom-emacs](https://github.com/hlissner/doom-emacs) - An Emacs framework for the stubborn martian hacker
- [dunst](https://dunst-project.org/) - Dunst notification Daemon
- [fd](https://github.com/sharkdp/fd) - A program to find entries in your filesystem
- [feh](https://github.com/derf/feh) - feh is a light-weight, configurable and versatile image viewer
- [gnu emacs](https://www.gnu.org/software/emacs/) - An extensible, customizable, free/libre text editor — and more
- [leftwm](https://github.com/leftwm/leftwm) - A window manager for adventurers written in Rust
- [mpd](https://github.com/MusicPlayerDaemon/MPD) - Music Player Daemon
- [mpv](https://github.com/mpv-player/mpv) - Media player
- [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp) - NCurses Music Player Client (Plus Plus)
- [neovim](https://github.com/neovim/neovim) - Vim-fork focused on extensibility and usability
- [picom](https://github.com/yshui/picom) - picom is a compositor for X, and a fork of Compton
- [qutebrowser](https://github.com/qutebrowser/qutebrowser) - Keyboard-driven, vim-like browser
- [ranger](https://github.com/ranger/ranger) - A VIM-inspired filemanager for the console
- [ripgrep](https://github.com/BurntSushi/ripgrep) - A line-oriented search tool that recursively searches the current directory for a regex pattern
- [rofi](https://github.com/davatorium/rofi) - Window switcher, application launcher and dmenu replacement
- [starship](https://github.com/starship/starship) - Shell prompt
- [xmobar](https://github.com/jaor/xmobar) - Status bar
- [xmonad](https://github.com/xmonad/xmonad) - Tiling window manager for X11
- [zathura](https://github.com/pwmt/zathura) - Document viewer
- [zoxide](https://github.com/ajeetdsouza/zoxide) - zoxide is a smarter cd command, inspired by z and autojump

# :penguin: Operating System
- :desktop_computer: [Arch Linux](https://archlinux.org/) - A lightweight and flexible Linux® distribution that tries to Keep It Simple.

# :nut_and_bolt: Gears
- [Razer DeathAdder V2 Mini](https://www2.razer.com/ap-en/store/razer-deathadder-v2-mini) - Gaming mouse
- [Razer Huntsman Tournament Edition - US - Black Keycaps](https://www.razer.com/gaming-keyboards/razer-huntsman-tournament-edition/RZ03-03080200-R3U1) - Gaming keyboard
